package kz.astana.serviceexample;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import androidx.core.app.NotificationCompat;

public class NotificationService extends Service {

    private String CHANNEL_ID = "CHANNEL_ID";

    private NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();
        Log.d("Hello", "onStart(Notification)");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Hello", "onStartCommand(Notification)");
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 10; i > 0; i--) {
                    try {
                        Log.d("Hello", "Count: " + i);
                        TimeUnit.SECONDS.sleep(1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                sendNotification();
                stopSelf();
            }
        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy(Notification)");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Notification channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription("Notification channel");
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void sendNotification() {
        Intent intent = new Intent(NotificationService.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                NotificationService.this,
                100,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        NotificationCompat.Builder builder = new NotificationCompat.Builder(NotificationService.this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle("Service");
        builder.setContentText("It's your notification text");
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);

        notificationManager.notify(1000, builder.build());
    }
}