package kz.astana.serviceexample;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;

public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int time = intent.getIntExtra("TIME_EXTRA", 0);
        String text = intent.getStringExtra("TEXT_EXTRA");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("Hello", "Start: " + text);
                for (int i = 1; i <= time; i++) {
                    try {
                        Log.d("Hello", text + ": " + i);
                        TimeUnit.SECONDS.sleep(1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.d("Hello", "End: " + text);
                stopSelf();
            }
        }).start();
    }
}
