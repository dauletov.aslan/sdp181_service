package kz.astana.serviceexample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Hello", "onCreate(Service)");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Hello", "onStartCommand(Service)");
        int time = intent.getIntExtra("EXTRA_TIME", 0);
        Log.d("Hello", "startId: " + startId);
        doSomeTask(time, startId);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy(Service)");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void doSomeTask(int time, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= time; i++) {
                    try {
                        Log.d("Hello", "startId: " + startId + ", i: " + i);
                        TimeUnit.SECONDS.sleep(1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                stopSelf(startId);
            }
        }).start();
    }
}