package kz.astana.serviceexample;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Hello", "onCreate(Activity)");
        setContentView(R.layout.activity_main);

        Button startService = findViewById(R.id.startService);
        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                intent.putExtra("EXTRA_TIME", 15);
                startService(intent);
            }
        });

        Button stopService = findViewById(R.id.stopService);
        stopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                stopService(intent);
            }
        });

        Button sendNotification = findViewById(R.id.sendNotification);
        sendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(MainActivity.this, NotificationService.class));
            }
        });

        Button intentService = findViewById(R.id.intentService);
        intentService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyIntentService.class);
                intent.putExtra("TIME_EXTRA", 7);
                intent.putExtra("TEXT_EXTRA", "One");
                startService(intent);
                intent.putExtra("TIME_EXTRA", 3);
                intent.putExtra("TEXT_EXTRA", "Two");
                startService(intent);
                intent.putExtra("TIME_EXTRA", 5);
                intent.putExtra("TEXT_EXTRA", "Three");
                startService(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Hello", "onResume(Activity)");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Hello", "onPause(Activity)");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy(Activity)");
    }
}